#oXy Test

This is a test application for oXy Online, s.r.o.

## Overview
WADL file for API service is in the ``public/service/`` directory, along with XSD describing entities

Application is written in Laravel 5.3 framework

Application is based around a REST API for registering and retrieving information about users. 

API wrapper for internal use is in the ``app/API`` directory.

API endpoint is in the ``app/Http/Controllers/api/V1`` directory

Everything related to persisting, retrieving and manipulating of User is located in ``app/Users`` directory

## Installation
 - Clone locally
 - copy .env.example to .env
 - Set USERS_API_URL in .env (must be accessible by HTTP)
 - Set MySQL db in .env (**the DB must be created prior to this**)
 - Run ``composer install`` && ``php artisan migrate`` && ``npm install`` && ``gulp`` && ``php artisan key:generate``

## Tests
Test are in ``./tests``  directory

Run them with ``phpunit``

## Ideas for improvement
 - Add middleware to return API responses in XML format
 - If needed, add JWT auth on API routes that gets info about users