<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRegister extends TestCase
{

	use DatabaseTransactions;

	public function setUp()
	{
		parent::setUp();
	}

	/** @test * */
	function it_can_display_registration_form()
	{
		$this->visitRoute('site.index')
			->see('oXy Test')
			->seeElement('form.register-form')
			->seeElement('input', ['name' => 'name', 'type' => 'text'])
			->seeElement('input', ['name' => 'email', 'type' => 'email'])
			->seeElement('input', ['name' => 'password', 'type' => 'password'])
			->seeElement('input', ['name' => 'is_admin', 'type' => 'checkbox'])
			->seeElement('button', ['type' => 'submit']);
	}

	/** @test * */
	function it_can_submit_registration_form_and_display_success_response()
	{
		/**
		 * Swap out the implementation of UsersApi for mock object, we cannot mock using phpunit,
		 * because the mocked object would not be called when selenium server submits the form
		 */
		$this->app->bind(
			\Acme\API\Users\Contracts\UsersApi::class,
			IntegrationTestRegistrationPassUsersApi::class
		);
		$this->visitRoute('site.index')
			->type('Lukas Benke', 'name')
			->type('benke.lukas@gmail.com', 'email')
			->type('supertajne_heslo', 'password')
			->check('is_admin')
			->press('Registrovat se')
			->seePageIs(route('site.index'))
			->seeElement('div.alert-success');
	}

	/** @test * */
	function it_can_submit_invalid_form_data_and_display_errors()
	{
		/**
		 * Swap out the implementation of UsersApi for mock object, we cannot mock using phpunit,
		 * because the mocked object would not be called when selenium server submits the form
		 */
		$this->app->bind(
			\Acme\API\Users\Contracts\UsersApi::class,
			IntegrationTestRegistrationFailedUsersApi::class
		);
		$this->visitRoute('site.index')
			//->type('Lukas Benke', 'name') - missing field, validation should fail
			->type('benke.lukas@gmail.com', 'email')
			->type('supertajne_heslo', 'password')
			->check('is_admin')
			->press('Registrovat se')
			->seePageIs(route('site.index'))
			->seeElement('div.alert-danger');
	}
}
