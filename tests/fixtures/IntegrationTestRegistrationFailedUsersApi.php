<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 5.12.16
 * Time: 13:06
 *
 * Mock implementation for integration tests, simulates failed registration
 */

class IntegrationTestRegistrationFailedUsersApi extends IntegrationTestRegistrationPassUsersApi implements \Acme\API\Users\Contracts\UsersApi
{

	public function register($data) : \Acme\API\Users\ValueObjects\UserValueObject
	{
		throw new \Acme\API\Exceptions\InvalidDataException(422, '{"name":["The name field is required"]}');
	}

}