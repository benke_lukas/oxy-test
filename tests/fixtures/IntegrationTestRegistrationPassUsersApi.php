<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 5.12.16
 * Time: 12:58
 *
 * Mock implementation for integration tests, simulates successful registration and implements all methods in contract
 */
use Acme\API\Users\Contracts\UsersApi;
use Acme\API\Users\ValueObjects\UserValueObject;
use Acme\User;

class IntegrationTestRegistrationPassUsersApi implements UsersApi
{

	public function register($data) : UserValueObject
	{
		$data['id'] = 1;
		return new UserValueObject($data);
	}

	public function get($id) : UserValueObject
	{
		$user = factory(User::class, 'normal_user')->make();
		$user['id'] = $id;

		return new UserValueObject($user->toArray());
	}

	public function all() : \Illuminate\Support\Collection
	{
		$users = factory(User::class, 'normal_user', 4)->make();
		$id = 1;
		$users = array_map(function($user) use(&$id){
			$user['id'] = $id;
			$id++;
			return new UserValueObject($user);
		}, $users);

		return $users;
	}

}