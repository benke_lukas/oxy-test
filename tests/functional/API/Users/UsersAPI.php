<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersAPI extends TestCase
{
	private $apiStub;

	public function setUp()
	{
		parent::setUp();

		$this->apiStub = $this->getMockBuilder(\Acme\API\Users\UsersApi::class)->getMock();
	}

	/** @test * */
	function it_can_register_user()
	{
		$data = [
			'id'       => 1,
			'name'     => 'Lukas Benke',
			'email'    => 'benke.lukas@gmail.com',
			'is_admin' => true
		];
		$this->apiStub->method('register')
			->willReturn(new \Acme\API\Users\ValueObjects\UserValueObject($data));

		$this->assertInstanceOf(\Acme\API\Users\ValueObjects\UserValueObject::class, $this->apiStub->register($data));
	}

	/** @test * */
	function it_throws_exception_if_invalid_data_provided_for_register_method()
	{
		$this->expectException(\Acme\API\Exceptions\InvalidDataException::class);

		$data = [
			//'name' => 'Lukas Benke', - missing field
			'email'    => 'benke.lukas@gmail.com',
			'password' => 'supertajne_heslo',
			'is_admin' => true
		];
		$this->apiStub->method('register')
			->will($this->throwException(new \Acme\API\Exceptions\InvalidDataException(422,
				'{"name":["The name field is required"]}')));
		$user = $this->apiStub->register($data);
	}

	/** @test * */
	function it_can_get_user()
	{
		$data = [
			'id'       => 1,
			'name'     => 'Lukas Benke',
			'email'    => 'benke.lukas@gmail.com',
			'is_admin' => true
		];
		$this->apiStub->method('get')
			->willReturn(new \Acme\API\Users\ValueObjects\UserValueObject($data));

		$user = $this->apiStub->get($data['id']);

		$this->assertInstanceOf(\Acme\API\Users\ValueObjects\UserValueObject::class, $user);
	}
}
