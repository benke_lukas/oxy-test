<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersController extends TestCase
{

	use DatabaseTransactions;

	/**
	 * @var \GuzzleHttp\Client $client
	 */
	private $client;

	public function setUp()
	{
		parent::setUp();
	}

	/**
	 * Creates mock client for guzzle, and adds response mock handler to stack,
	 * so we can test the response without actually calling the API endpoint
	 *
	 * @param $response
	 * @return \GuzzleHttp\Client
	 */
	private function createMockClient($response) : \GuzzleHttp\Client {
		$mock = new \GuzzleHttp\Handler\MockHandler(
			[
				$response
			]
		);
		$handler = new \GuzzleHttp\HandlerStack($mock);

		$client = new \GuzzleHttp\Client([
			'handler' => $handler
		]);

		return $client;
	}

	/** @test * */
	function it_can_create_user_and_return_location_header()
	{
		$user       = factory(\Acme\User::class, 'normal_user')->make([
			'name'     => 'Lukas Benke',
			'email'    => 'benke.lukas@gmail.com',
			'password' => 'nejake_supertajne_heslo',
			'is_admin' => false
		]);
		$user['id'] = 1;

		$client = $this->createMockClient(new \GuzzleHttp\Psr7\Response(201, [
			'Location' => 'http://oxy-test.dev/api/V1/users/1'
		], json_encode($user)));

		$response = $client->post(route('api.users.create'), [
			'name'     => 'Lukas Benke',
			'email'    => 'benke.lukas@gmail.com',
			'password' => 'nejake_supertajne_heslo',
			'is_admin' => false
		]);
		$data     = $this->parseJson($response);

		$this->assertEquals(201, $response->getStatusCode());
		$this->assertEquals('Lukas Benke', $data->name);
		$this->assertEquals('benke.lukas@gmail.com', $data->email);
		$this->assertEquals(false, $data->is_admin);

		$this->assertEquals('http://oxy-test.dev/api/V1/users/1', $response->getHeader('Location')[0]);
	}

	/** @test * */
	function it_can_get_one_user()
	{
		$user       = factory(\Acme\User::class, 'normal_user')->make();
		$user['id'] = 2;

		$client = $this->createMockClient(new \GuzzleHttp\Psr7\Response(200, [], json_encode($user)));

		$response = $client->get(route('api.users.show', 1));

		$data = $this->parseJson($response);

		$this->assertEquals(200, $response->getStatusCode());
		$this->assertIsJson($data);
	}

	/** @test * */
	function it_can_get_all_users()
	{
		$users = factory(\Acme\User::class, 'normal_user', 4)->make();

		$client = $this->createMockClient(new \GuzzleHttp\Psr7\Response(200, [], json_encode($users)));

		$response = $client->get(route('api.users.index'));

		$data = $this->parseJson($response);

		$this->assertEquals(200, $response->getStatusCode());

		$this->assertEquals(4, count($data));
	}
}
