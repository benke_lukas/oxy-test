<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRepository extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    /**
     * @var \Acme\Users\Repositories\Contracts\UserRepository $userRepository
     */
    private $userRepository;

	public function setUp()
	{
		parent::setUp();

		$this->userRepository = app(\Acme\Users\Repositories\Contracts\UserRepository::class);
    }

    /** @test **/
    function it_can_register_admin_user() {
		$user = $this->userRepository->register('Lukas Benke', 'benke.lukas@gmail.com', 'supertajne_heslo', true);

		$this->assertInstanceOf(\Acme\User::class, $user);
		$this->assertEquals($user->name, 'Lukas Benke');
		$this->assertEquals($user->email, 'benke.lukas@gmail.com');
		$this->assertEquals($user->is_admin, true);
    }
    /** @test **/
    function it_can_register_normal_user() {
		$user = $this->userRepository->register('Lukas Benke', 'benke.lukas@gmail.com', 'supertajne_heslo', false);

		$this->assertInstanceOf(\Acme\User::class, $user);
		$this->assertEquals($user->name, 'Lukas Benke');
		$this->assertEquals($user->email, 'benke.lukas@gmail.com');
		$this->assertEquals($user->is_admin, false);
    }

    /** @test **/
    function it_can_get_one_user() {
        $user = factory(\Acme\User::class, 'normal_user')->create();

        $userFromRepository = $this->userRepository->getById($user->id);

        $this->assertInstanceOf(\Acme\User::class, $userFromRepository);
    }
    /** @test **/
    function it_can_get_all_users() {
        $users = factory(\Acme\User::class, 'normal_user', 3)->create();

        $this->assertInstanceOf(\Illuminate\Support\Collection::class, $users);
        $this->assertEquals(3, $users->count());
    }
}
