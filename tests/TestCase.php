<?php

abstract class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    protected function parseJson(\GuzzleHttp\Psr7\Response $response) {
    	return \GuzzleHttp\json_decode($response->getBody());
    }

    protected function assertIsJson($data) {
	    $this->assertEquals(0, json_last_error());
    }
}
