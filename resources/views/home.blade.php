<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{asset('css/app.css')}}" type="text/css">

    <title>oXy test</title>
</head>
<body>
<h1 class="text-center">oXy Test</h1>
<div class="container">
    <div class="col-md-6 col-md-offset-3">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if ( session()->has('success') )
            <div class="alert alert-success">
                Uživatel vytvořen.
            </div>
        @endif
        <form action="{{route('site.user.register')}}" class="register-form" method="POST" role="form">
            {{csrf_field()}}
            <legend>Registrovat uživatele</legend>

            <div class="form-group">
                <label for="name">Jméno</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Jméno">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
            </div>
            <div class="form-group">
                <label for="password">Heslo</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Heslo">
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="is_admin" value="1" id="is_admin">
                    Administrátor
                </label>
            </div>

            <button type="submit" class="btn btn-primary">Registrovat se</button>
        </form>
    </div>
</div>
</body>
</html>
