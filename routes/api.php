<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'users'], function(){
	Route::get('/', ['as' => 'api.users.index', 'uses' => 'UsersController@index']);
	Route::post('/', ['as' => 'api.users.create', 'uses' => 'UsersController@store']);
	Route::get('/{user}', ['as' => 'api.users.show', 'uses' => 'UsersController@show']);
});
