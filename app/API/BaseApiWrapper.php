<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2.12.16
 * Time: 15:14
 */

namespace Acme\API;


use GuzzleHttp\Client;

abstract class BaseApiWrapper
{

	/**
	 * @var Client $client
	 */
	protected $client;

	private $base_uri;

	/**
	 * BaseApiWrapper constructor.
	 * @param $base_uri
	 */
	public function __construct(string $base_uri)
	{
		$this->base_uri = $base_uri;
		$this->createClient();
	}

	private function createClient()
	{
		$this->client = new Client([
			'base_uri' => $this->base_uri,
			'headers'  => [
				'Accept' => 'application/json',
			]
		]);
	}
}