<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 5.12.16
 * Time: 9:45
 */

namespace Acme\API\Exceptions;


use Exception;
use GuzzleHttp\Exception\ClientException;

class InvalidDataException extends \Exception
{
	private $errors;

	public function __construct(int $statusCode, string $json_errors)
	{
		parent::__construct("Invalid data provided", $statusCode, null);
		try {
			$this->errors = \GuzzleHttp\json_decode($json_errors, true);
		} catch (\InvalidArgumentException $e) {
			$this->errors = [];
		}
	}

	/**
	 * @return mixed
	 */
	public function getErrors() : array
	{
		return $this->errors;
	}

}