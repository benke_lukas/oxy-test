<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2.12.16
 * Time: 15:12
 */

namespace Acme\API\Users\Contracts;


use Acme\API\Users\ValueObjects\UserValueObject;
use Illuminate\Support\Collection;

interface UsersApi
{
	public function register($data) : UserValueObject;
	public function get($id) : UserValueObject;
	public function all() : Collection;
}