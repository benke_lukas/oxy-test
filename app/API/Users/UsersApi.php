<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2.12.16
 * Time: 15:14
 */

namespace Acme\API\Users;


use Acme\API\BaseApiWrapper;
use Acme\API\Exceptions\InvalidDataException;
use Acme\API\Users\Contracts\UsersApi as UsersApiContract;
use Acme\API\Users\ValueObjects\UserValueObject;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;

class UsersApi extends BaseApiWrapper implements UsersApiContract
{


	/**
	 * UsersApi constructor.
	 */
	public function __construct()
	{
		parent::__construct(env('USERS_API_URL'));
	}

	public function register($data): UserValueObject
	{
		try {
			$response = $this->client->request('POST', '', [
				'form_params' => $data
			]);

			$data = \GuzzleHttp\json_decode($response->getBody(), true);

			$user = new UserValueObject($data);

			return $user;
		} catch (ClientException $e) {
			if ($e->getResponse()->getStatusCode() == 422) {
				throw new InvalidDataException($e->getResponse()->getStatusCode(), $e->getResponse()->getBody());
			}
			throw $e;
		}
	}

	public function get($id): UserValueObject
	{
		$response = $this->client->get("users/{$id}");

		$data = \GuzzleHttp\json_decode($response->getBody(), true);

		$user = new UserValueObject($data);

		return $user;
	}

	public function all(): Collection
	{
		$response = $this->client->get("/");

		$data = \GuzzleHttp\json_decode($response->getBody(), true);

		$collection = new Collection();
		foreach ( $data as $user ) {
			$collection->push(new UserValueObject($user));
		}

		return $collection;
	}

}