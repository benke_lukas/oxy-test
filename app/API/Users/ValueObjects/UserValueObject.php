<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 5.12.16
 * Time: 9:35
 */

namespace Acme\API\Users\ValueObjects;


final class UserValueObject
{

	private $id;
	private $name;
	private $email;

	private $is_admin;

	/**
	 * UserValueObject constructor.
	 * @param array $data
	 */
	public function __construct(array $data)
	{
		$this->id       = $data['id'];
		$this->name     = $data['name'];
		$this->email    = $data['email'];
		$this->is_admin = $data['is_admin'];
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}
	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}
	/**
	 * @return mixed
	 */
	public function getIsAdmin()
	{
		return $this->is_admin;
	}

}