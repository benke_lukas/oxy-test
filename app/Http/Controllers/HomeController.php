<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2.12.16
 * Time: 15:34
 */

namespace Acme\Http\Controllers;


class HomeController extends Controller
{
	public function index() {
		return view('home');
	}
}