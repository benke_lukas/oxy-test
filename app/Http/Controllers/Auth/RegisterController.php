<?php

namespace Acme\Http\Controllers\Auth;

use Acme\API\Exceptions\InvalidDataException;
use Acme\API\Users\Contracts\UsersApi;
use Acme\Http\Controllers\Controller;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

	/**
	 * @var UsersApi
	 */
	private $usersApi;

	/**
	 * RegisterController constructor.
	 * @param UsersApi $usersApi
	 */
	public function __construct(UsersApi $usersApi)
	{
		$this->usersApi = $usersApi;
	}

	public function register(Request $request)
	{
		try {
			$user = $this->usersApi->register($request->all());

			return redirect()->back()->with([
				'success' => true,
				'user'    => $user
			]);
		} catch (InvalidDataException $e) {
			return redirect()->back()->withErrors($e->getErrors());
		}
	}
}
