<?php

namespace Acme\Http\Controllers\api\V1;

use Acme\Http\api\BaseApiController;
use Acme\Http\Controllers\Controller;
use Acme\Users\Repositories\Contracts\UserRepository;
use Acme\Users\Requests\CreateUser;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UsersController extends Controller
{

	/**
	 * @var UserRepository
	 */
	private $userRepository;

	/**
	 * UsersController constructor.
	 * @param UserRepository $userRepository
	 */
	public function __construct(
		UserRepository $userRepository
	)
	{
		$this->userRepository = $userRepository;
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userRepository->getAll();

        return response()->json($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateUser  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUser $request)
    {
        $user = $this->userRepository->register(
        	$request->get('name'),
	        $request->get('email'),
	        $request->get('password'),
	        $request->get('is_admin', false)
        );

        return response()->json(
        	$user,
	        200,
	        [
		        'Location' => route('api.users.show', $user->id)
	        ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        try {
			$user = $this->userRepository->getById($id);

			return response()->json($user, 200);
        } catch ( ModelNotFoundException $e ) {
			return response()->json("User with id {$id} not found", 404);
        }
    }
}
