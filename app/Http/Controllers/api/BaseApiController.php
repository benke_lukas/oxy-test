<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2.12.16
 * Time: 10:52
 */

namespace Acme\Http\api;


use Acme\Http\Controllers\Controller;

class BaseApiController extends Controller
{
	protected $allowedResponseTypes = [
		'json',
		'xml'
	];
	public function sendResponse($data, $status = 200, $type = 'json') {
		$response = response();
		if ( $type == 'json' ) {
			return $response->json($data, $status);
		}
	}
}