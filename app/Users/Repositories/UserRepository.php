<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2.12.16
 * Time: 10:59
 */

namespace Acme\Users\Repositories;


use Acme\User;
use Acme\Users\Repositories\Contracts\UserRepository as UserRepositoryContract;
use Illuminate\Support\Collection;

class UserRepository implements UserRepositoryContract
{

	/**
	 * @var User
	 */
	private $model;

	/**
	 * UserRepository constructor.
	 * @param User $model
	 */
	public function __construct(User $model)
	{
		$this->model = $model;
	}

	public function register(string $name, string $email, string $password, bool $is_admin = false): User
	{
		$user = $this->model->create([
			'name' => $name,
			'email' => $email,
			'password' => bcrypt($password),
			'is_admin' => $is_admin
		]);

		return $user;
	}

	public function getById(int $id) : User
	{
		return $this->model->findOrFail($id);
	}

	public function getAll() : Collection
	{
		return $this->model->all();
	}
}