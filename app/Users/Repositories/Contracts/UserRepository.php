<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 2.12.16
 * Time: 10:57
 */

namespace Acme\Users\Repositories\Contracts;


use Acme\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface UserRepository
{

	/**
	 * Registers user
	 *
	 * @param string $name
	 * @param string $email
	 * @param string $password
	 * @param bool $is_admin
	 * @return User
	 */
	public function register(string $name, string $email, string $password, bool $is_admin = false) : User;

	/**
	 * Gets user by id
	 *
	 * @param int $id
	 * @return mixed
	 * @throws ModelNotFoundException
	 */
	public function getById(int $id) : User;

	/**
	 * Gets all user in system
	 *
	 * @return Collection
	 */
	public function getAll() : Collection;
}