<?php

namespace Acme\Users\Providers;

use Acme\API\Users\Contracts\UsersApi as UsersApiContract;
use Acme\API\Users\UsersApi;
use Acme\Users\Repositories\Contracts\UserRepository as UserRepositoryContract;
use Acme\Users\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
        	UserRepositoryContract::class,
	        UserRepository::class
        );
        $this->app->bind(
        	UsersApiContract::class,
	        UsersApi::class
        );
    }
}
